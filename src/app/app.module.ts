import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IfviewportDirective } from './ifviewport.directive';
import { WidthService } from './width.service';
import { TestComponent } from './test/test.component';

export interface IConfig {
  small: number;
  medium: number;
  large: number;
}

@NgModule({
  declarations: [
    AppComponent,
    IfviewportDirective,
    TestComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [WidthService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
