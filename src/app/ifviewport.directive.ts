import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { WidthService } from './width.service';

@Directive({
  selector: '[ifViewportSize]'
})
export class IfviewportDirective {

  constructor(private templateRef: TemplateRef<any>, 
              private viewContainer: ViewContainerRef,
              private width: WidthService) 
  {}

  @Input() set ifViewportSize(width: string) {
    window.addEventListener('resize', () => {
      if (width === this.width.getScale()) {
        this.viewContainer.clear();
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    });
  }
}

