import { Injectable } from '@angular/core';
import { IConfig } from './app.module';

@Injectable({
  providedIn: 'root'
})
export class WidthService {

  public config: IConfig = {
    small: 400,
    medium: 700,
    large: 1000
  }

  scale: string;



  getScale() {
    if (window.innerWidth >= this.config.small && window.innerWidth < this.config.medium) {
      this.scale = 'small';
    } else if (window.innerWidth >= this.config.medium && window.innerWidth < this.config.large) {
      this.scale = 'medium';
    } else if(window.innerWidth >= this.config.large) {
      this.scale = 'large';
    } else {
      this.scale = 'extaSmall'
    }
    return this.scale;
  }

}
